package t3_201820;

import junit.framework.TestCase;
import model.data_structures.LinkedListQueue;

public class LinkedListQueueTest extends TestCase{

	private LinkedListQueue<Integer> cola;
	
	public void setupEscenario1()
	{
		cola = new LinkedListQueue<>();
		for(int i=0; i<100; i++){
			cola.enqueue(i);
		}
	}
	
	public void testIsEmpty1(){
		setupEscenario1();
		assertEquals(false, cola.isEmpty());
	}
	
	public void testIsEmpty2(){
		cola = new LinkedListQueue<Integer>();
		assertEquals(true, cola.isEmpty());
	}
	
	
	public void testEnqueue1(){
		setupEscenario1();
		assertEquals(new Integer(99), cola.getLast());
		assertEquals(new Integer(0), cola.getFirst());
	}
	
	public void testEnqueue2(){
		setupEscenario1();
		for(int i=100; i<150; i++){
			cola.enqueue(i);
		}
		cola.dequeue();
		cola.dequeue();
		cola.dequeue();
		assertEquals(new Integer(3), cola.getFirst());
		assertEquals(new Integer(149), cola.getLast());
	}
	public void testSize(){
		setupEscenario1();
		for(int i = 0; i<10; i++)
		{
			cola.dequeue();
		}
		assertEquals(90, cola.size());
	}
	
	public void testDequeue(){
		setupEscenario1();
		for(int i=0; i<20; i++){
			cola.dequeue();
		}
		assertEquals(new Integer(20), cola.dequeue());
	}
	
	public void testGetFirstGetLast(){
		setupEscenario1();
		for(int i=0; i<10; i++){
			cola.dequeue();
		}
		for(int i=100; i<120; i++){
			cola.enqueue(i);
		}
		assertEquals(new Integer(10), cola.getFirst());
		assertEquals(new Integer(119), cola.getLast());
		
	}
	
}
