package model.data_structures;

import java.util.Iterator;




public class LinkedListQueue<T> implements IQueue<T>{

	private Node<T> first;
	private Node<T> last;
	private int size=0;
	@Override
	public Iterator<T> iterator() {
		return new ListIterator<T>(first);
	}
	private class ListIterator<T> implements Iterator<T>{
		Node<T> current;
		public ListIterator(Node<T> first){
			current = first;
		}
		public boolean hasNext(){
			return current != null;
		}
		public T next(){
			T elemento = null;
			if(current != null){
				elemento = current.getItem();
				current = current.getNext();
			}
			return elemento;
		}
		@Override
		public void remove() {			
		}
	}

	@Override
	public boolean isEmpty() {
		return size==0;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void enqueue(T t) {
		Node<T> newNode = new Node<>(t);
		if(size==0){
			first = newNode;
			last = newNode;
		}
		else {
			Node<T> oldLast = last;
			oldLast.setNext(newNode);
			last = newNode;
		}
		size++;	
	}

	@Override
	public T dequeue() {
		if(size == 0){
			return null;
		}
		Node<T> oldFirst = first;
		T item = first.getItem();
		first = oldFirst.getNext();
		oldFirst.setNext(null);
		size--;
		return item;
	}
	
	public T getFirst(){
		return first.getItem();
	}
	
	public T getLast(){
		return last.getItem();
	}
	
}
