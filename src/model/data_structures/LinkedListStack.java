package model.data_structures;

import java.util.Iterator;

public class LinkedListStack<T> implements IStack<T>{

	private Node<T> top;
	private int size = 0;
	
	@Override
	public Iterator<T> iterator() {
		return new ListIterator<T>(top);
	}
	private class ListIterator<T> implements Iterator<T>{
		Node<T> current;
		public ListIterator(Node<T> first){
			current = first;
		}
		public boolean hasNext(){
			return current != null;
		}
		public T next(){
			T elemento = null;
			if(current != null){
				elemento = current.getItem();
				current = current.getNext();
			}
			return elemento;
		}
		@Override
		public void remove() {			
		}
	}

	@Override
	public boolean isEmpty() {
		return (size==0);
	}

	@Override
	public int size() {
		return size; 
	}

	@Override
	public void push(T t) {
		Node<T> newNode = new Node<T>(t);
		if(top == null){
			top = newNode;
		}
		else{
			newNode.setNext(top);
			top = newNode;
		}
		size++;
	}

	@Override
	public T pop(){
		if(top == null){
			return null;
		}
		T element = top.getItem();
		Node<T> nextTop = top.getNext();
		top.setNext(null);
		top = nextTop;
		size--;
		return element;		
	}
	
	public T getTop(){
		return top.getItem();
	}
	
}
