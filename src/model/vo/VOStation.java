package model.vo;

public class VOStation {
	
	private int id;
	private String name;
	private String city;
	private double latitude;
	private double longitude;
	private int dpcapacity;
	private String online_date;
	
	public VOStation(int id, String name, String city, double latitude, double longitude, int dpcapacity,
			String online_date) {
		this.id = id;
		this.name = name;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dpcapacity = dpcapacity;
		this.online_date = online_date;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public int getDpcapacity() {
		return dpcapacity;
	}
	public void setDpcapacity(int dpcapacity) {
		this.dpcapacity = dpcapacity;
	}
	public String getOnline_date() {
		return online_date;
	}
	public void setOnline_date(String online_date) {
		this.online_date = online_date;
	}
	
	
	
}
