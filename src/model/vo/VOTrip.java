package model.vo;

import model.data_structures.LinkedListQueue;
import model.data_structures.Node;

/**
 * Representation of a Trip object
 */
public class VOTrip  {

	private int trip_id;
	private String start_time;
	private String end_time;
	private int bikeId;
	private double tripDuration;
	private int from_station_id;
	private String from_station_name;
	private int to_station_id;
	private String to_station_name;
	private String userType;
	private String gender;
	private int birthyear;
	
	
	public VOTrip(int trip_id, String start_time, String end_time, int bikeId,
			double tripDuration, int from_station_id, String from_station_name,
			int to_station_id, String to_station_name, String userType,
			String gender, int birthyear) {
		
		this.trip_id = trip_id;
		this.start_time = start_time;
		this.end_time = end_time;
		this.bikeId = bikeId;
		this.tripDuration = tripDuration;
		this.from_station_id = from_station_id;
		this.from_station_name = from_station_name;
		this.to_station_id = to_station_id;
		this.to_station_name = to_station_name;
		this.userType = userType;
		this.gender = gender;
		this.birthyear = birthyear;
	}
	
	
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return 0;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return "";
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return "";
	}


	public int getTrip_id() {
		return trip_id;
	}


	public void setTrip_id(int trip_id) {
		this.trip_id = trip_id;
	}


	public String getStart_time() {
		return start_time;
	}


	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}


	public String getEnd_time() {
		return end_time;
	}


	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}


	public int getBikeId() {
		return bikeId;
	}


	public void setBikeId(int bikeId) {
		this.bikeId = bikeId;
	}


	public double getTripDuration() {
		return tripDuration;
	}


	public void setTripDuration(double tripDuration) {
		this.tripDuration = tripDuration;
	}


	public int getFrom_station_id() {
		return from_station_id;
	}


	public void setFrom_station_id(int from_station_id) {
		this.from_station_id = from_station_id;
	}


	public String getFrom_station_name() {
		return from_station_name;
	}


	public void setFrom_station_name(String from_station_name) {
		this.from_station_name = from_station_name;
	}


	public int getTo_station_id() {
		return to_station_id;
	}


	public void setTo_station_id(int to_station_id) {
		this.to_station_id = to_station_id;
	}


	public String getTo_station_name() {
		return to_station_name;
	}


	public void setTo_station_name(String to_station_name) {
		this.to_station_name = to_station_name;
	}


	public String getUserType() {
		return userType;
	}


	public void setUserType(String userType) {
		this.userType = userType;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public int getBirthyear() {
		return birthyear;
	}


	public void setBirthyear(int birthyear) {
		this.birthyear = birthyear;
	}
}
